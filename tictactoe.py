from tkinter import *
import random

def new_game():
    global round
    round += 1
    label.config(text="Round "+ str(round))
    new_button.config(background="white")
    for row in range(3):
        for col in range(3):
            field[row][col]['text'] = ' '
            field[row][col]['background'] = 'lavender'
    global game_status
    game_status = True
    global move_count
    move_count = 0

def next_turn(row, col):
    if game_status and field[row][col]['text'] == ' ':
        field[row][col]['text'] = 'X'
        global move_count
        move_count += 1
        check_win('X')
        #empty_spaces()
        if game_status and move_count < 5:
            computer_move()
            check_win('O')

def empty_spaces():
    pass

def check_win(player):
    for n in range(3):
        check_win_line(field[n][0], field[n][1], field[n][2], player)
        check_win_line(field[0][n], field[1][n], field[2][n], player)
    check_win_line(field[0][0], field[1][1], field[2][2], player)
    check_win_line(field[2][0], field[1][1], field[0][2], player)

def check_win_line(pos1, pos2, pos3, player):
    global computer_score
    global user_score
    if pos1['text'] == player and pos2['text'] == player and pos3['text'] == player:
        pos1['background'] = pos2['background'] = pos3['background'] = 'red'
        if player == "X":
            user_score+=1
            label.config(text="You Won!")
            new_button.config(background="green")
        elif player == "O":
            computer_score+=1
            label.config(text="You Lose!")
            new_button.config(background="green")
        global game_status
        game_status = False
        score_label.config(text="Score: You - "+str(user_score)+" AI - "+str(computer_score))
    elif move_count == 5 and game_status is True:
        label.config(text="Tie!")
        new_button.config(background="green")
    
def move_to_win(a1,a2,a3,smb):
    res = False
    if a1['text'] == smb and a2['text'] == smb and a3['text'] == ' ':
        a3['text'] = 'O'
        res = True
    if a1['text'] == smb and a2['text'] == ' ' and a3['text'] == smb:
        a2['text'] = 'O'
        res = True
    if a1['text'] == ' ' and a2['text'] == smb and a3['text'] == smb:
        a1['text'] = 'O'
        res = True
    return res

def computer_move():
    #O can win
    for n in range(3):
        if move_to_win(field[n][0], field[n][1], field[n][2], 'O'):
            return
        if move_to_win(field[0][n], field[1][n], field[2][n], 'O'):
            return
    if move_to_win(field[0][0], field[1][1], field[2][2], 'O'):
        return
    if move_to_win(field[2][0], field[1][1], field[0][2], 'O'):
        return
    #x can win
    for n in range(3):
        if move_to_win(field[n][0], field[n][1], field[n][2], 'X'):
            return
        if move_to_win(field[0][n], field[1][n], field[2][n], 'X'):
            return
    if move_to_win(field[0][0], field[1][1], field[2][2], 'X'):
        return
    if move_to_win(field[2][0], field[1][1], field[0][2], 'X'):
        return
    while True:
        row = random.randint(0, 2)
        col = random.randint(0, 2)
        if field[row][col]['text'] == ' ':
            field[row][col]['text'] = 'O'
            break

mainwindow = Tk()
mainwindow.title("Tic-Tac-Toe")
game_status = True
field = []
move_count = 0
round = 1
user_score = computer_score = 0
label = Label(text="Round "+ str(round), font=('Courier', 30))
label.pack(side="top")
score_label = Label(text="Score: You - " + str(user_score) + " AI - " + str(computer_score), font=('Courier', 15,'bold'))
score_label.pack(side="top")
frame = Frame(mainwindow)
frame.pack()
new_button = Button(text='New game!', command=new_game, font=('Courier', 15))
new_button.pack(side="top")

for row in range(3):
    buttons = []
    for col in range(3):
        button = Button(frame, text=' ', width=6, height=3,
                        font=('Courier', 20, 'bold'),
                        background='lavender',
                        command=lambda row=row, col=col: next_turn(row, col))
        button.grid(row=row, column=col)
        buttons.append(button)
    field.append(buttons)

mainwindow.mainloop()